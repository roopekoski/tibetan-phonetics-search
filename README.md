# Tibetan Phonetics Search
## Use case
User types "Khyentse Wangpo" and finds "mkhyen brtse'i dbang po"

## Benefits
Great convenience for Western users who know Tibetan but have difficulties with ortography.

## The Concept
Both phonetics and Wylie are converted to an extremely lenient middle format, which produces exact matches in search. The leniency will not produce too many false matches, because the syllables of each word are kept together and all the words are required.

Splitting of phonetic words to syllables is done by first assuming that each syllable has one vowel, and then finding a consonant pair between vowels, which is statistically least likely to occur together. This has worked very well in testing.

## Quality and Shortcomings
In standard Tibetan, the accuracy seems to be well above 95%. Some irregularly pronounced words don't necessarily work.

When Wylie contains Sanskrit, this method fails at least 50% of the time.  Some very common loan words like "padma" have been coded as exceptions".  In practice, queries with mixed Sanskrit and Tibetan phonetics are often matched by other parts of the query in Sanskrit or English fields.

Users will likely understand the shortcomings and intuitively switch to Tibetan or Wylie when necessary.

Highlighting does not work yet.

## Testing
At the end of `fonetix.py` there is a list of phonetix-Wylie pairs.  Add your own and run the script `python3 fonetix.py`

## Indexing (in Python)
Create a normal text field for the middle format in your Elasticsearch mappings.

Copy `fonetix.py` in the same directory with your indexer and import the necessary functions:<br/>
`from fonetix import convert_to_fonetix, wylie_to_phonetics`

While indexing, do:
`middle_format = convert_to_fonetix(wylie_to_phonetics(wylie))`<br/>
and index the `middle_format`.

## Search API
In your search API, copy `fonetix.py` in the same directory with the API and import the necessary functions:

`from fonetix import fonetix_json, query_to_syllables, convert_to_fonetix`

Convert the query string to Elasticsearch JSON:

`elastic_json = fonetix_json(query_to_syllables(convert_to_fonetix(query_string))`

Insert this JSON to your Elasticsearch query.

## Explanations of each function
### convert_to_fonetix()
The main leniency happens here.  It takes in all kinds of phonetics and converts them to the middle format.

### wylie_to_phonetics()
Converts Wylie to something that looks a bit like real phonetics, but in separate syllables.

### query_to_syllables()
Cuts phonetics into syllables.  It assumes that each syllable has one vowel and then looks for the statistically least probable character pairs to appear together between those vowels.

### fonetix_json()
Creates combinations to handle the last remaining irregularities  with "pal"/"pel" and "a'i", and creates ORs (`should`) of each word.  It then combines the words into one AND (`must`), so that each query word is required, and individual syllables will not match alone.

Elastic JSON example:
```
Wylie: "'jam dbyangs mkhyen brtse'i dbang po"
Phonetics: "Jamyang Khyentse Wangpo"
Elasticsearch query:
{"bool": 
    {"must": [
      {"bool": 
        {"should": [
            {"match": {"fonetix": "jam yang"}}
          ]
        }
      },
      {"bool": {
        "should": [
        {"match": {"fonetix": "gyen dse"}},
        {"match": {"fonetix": "gyen dsai"}},
        {"match": {"fonetix": "gyan dse"}},
        {"match": {"fonetix": "gyan dsai"}}
        ]
      }},
      {"bool": {"should": [
        {"match": {"fonetix": "bang bo"}}
      ]
      }}
    ]
}}
```

